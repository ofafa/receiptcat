# -*- coding:utf-8 -*-

class Cat:


    def __init__(self):
        import cv2
        print 'My name is 小芝麻!'
        self._eyes = cv2.VideoCapture(0)

    def eat(self, instance=None, verbose=False):
        pass

    def eye_capture(self):
        import cv2
        ret, image = self._eyes.read()
        cv2.imshow('Hello', image)
        cv2.imwrite('../test/cap.jpg',image)

    def close_eyes(self):
        import cv2
        self._eyes.release()
        cv2.destroyAllWindows()

    def look(self, inputstring):
        """
        >>>cat.eat('../test/default.txt',verbose=True)
        Month: None
        12345678: 0
        22345678: 20000000
        32345678: 1000000
        42345678: 1000000
        *****000: 200
        *****123: 200
        *****456: 200

        >>>cat.look('12345678')
        miao! You got $0!
        """

        #Detect the length & format of input string
        pass
        #OCR
        content = self.ocr(inputstring)
        number  = self.cleanup(content)
        print 'I am watching you!'
        print 'You are ', number, '!'

    def miao(self):
        print 'miao'

    def ocr(self, image=None):
        import Image
        import pytesseract
        return pytesseract.image_to_string(Image.open(image))

    def cleanup(self, content):
        tokens = content.split('\n')
        for token in tokens:
            if len(token) == 8 and token.isdigit():
                return token


if __name__ == '__main__':
    import doctest
    doctest.testmod(extraglobs={'cat':Cat()})


